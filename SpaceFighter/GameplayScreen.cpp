
#include "GameplayScreen.h"
#include "Level.h"
#include "Level01.h"
#include "Level02.h"
#include "GameObject.h"


// Move the contents of gameplay screen into a public load level function taking level index
// call load level function in constructor
// load level will unload level

// check if m_plevel is a nullptr if it is delete it

// Adding comment for demo

GameplayScreen::GameplayScreen(const int levelIndex)
{
	LoadLevel(levelIndex);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show();
}

void GameplayScreen::LoadLevel(const int levelIndex)
{
	if (m_pLevel) delete m_pLevel;

	switch (levelIndex)
	{
		case 0:
			m_pLevel = new Level01();
			break;
		case 1:
			m_pLevel = new Level02();
			break;
	}

	m_pLevel->SetGameplayScreen(this);

	if(m_pResourceManager)
		m_pLevel->LoadContent(m_pResourceManager);
}

void GameplayScreen::LoadContent(ResourceManager *pResourceManager)
{
	m_pLevel->LoadContent(pResourceManager);
	m_pResourceManager = pResourceManager;
}

void GameplayScreen::HandleInput(const InputState *pInput)
{
	m_pLevel->HandleInput(pInput);
}

void GameplayScreen::Update(const GameTime *pGameTime)
{
	m_pLevel->Update(pGameTime);
	if (m_pLevel->GetCount() == 21)
	{
		double keepScore = m_pLevel->GetScore();
		m_pLevel->UnloadContent();
		//could also implement for() loop to add extra levels
		LoadLevel(1);
		GameObject::GetCurrentLevel()->SetScore(keepScore);
		GameObject::GetCurrentLevel()->SetLevel(1);
	

	}
}

void GameplayScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();

	m_pLevel->Draw(pSpriteBatch);

	pSpriteBatch->End();
}
