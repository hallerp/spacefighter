
#pragma once

#include "KatanaEngine.h"

using namespace KatanaEngine;

class Level;

class GameplayScreen : public Screen
{

public:

	GameplayScreen(const int levelIndex = 0);

	virtual ~GameplayScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void HandleInput(const InputState *pInput);

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual bool IsQuittingLevel() { return m_isQuittingLevel; }

	virtual void LoadLevel(const int levelIndex);

	virtual ResourceManager *GetResource() const { return m_pResourceManager; }


private:

	Level *m_pLevel = nullptr;

	bool* m_isQuittingLevel = false;

	ResourceManager* m_pResourceManager = nullptr;
};
