
#pragma once

#include "KatanaEngine.h"
#include "PlayerShip.h"
#include "CollisionManager.h"

using namespace KatanaEngine;

class GameplayScreen;

class Level
{

public:

	Level();
	virtual ~Level();

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() = 0;

	virtual void HandleInput(const InputState *pInput);

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void AddGameObject(GameObject *pGameObject) { m_gameObjects.push_back(pGameObject); }

	virtual void UpdateSectorPosition(GameObject *pGameObject);

	virtual void SetGameplayScreen(GameplayScreen* pGameplayScreen) { m_pGameplayScreen = pGameplayScreen; }

	virtual GameplayScreen *GetGameplayScreen() const { return m_pGameplayScreen; }

	virtual void IncrementCount(double count) { m_counter += count; }
	virtual void IncrementScore(double count) { m_ScoreCount += count; }
	virtual double GetCount() const { return m_counter; }

	virtual double GetScore() const { return m_ScoreCount; }
	virtual void SetLevel(int currentLevel) { m_currentLevel = currentLevel; }
	virtual void SetScore(int currentScore) { m_ScoreCount = currentScore; }


	virtual int GetLevel() const { return m_currentLevel; }
	

	template <typename T>
	T *GetClosestObject(const Vector2 position, const float range)
	{
		float squaredRange = range * range;
		if (range <= 0)
		{
			int w = Game::GetScreenWidth();
			int h = Game::GetScreenHeight();
			squaredRange = w * w + h * h;
		}
		float squaredDistance;

		std::vector<GameObject *>::iterator m_it = m_gameObjects.begin();
		T *pClosest = nullptr;

		for (; m_it != m_gameObjects.end(); m_it++)
		{
			GameObject *pGameObject = *m_it;
			if (pGameObject->IsActive()) continue;

			squaredDistance = (position - pGameObject->GetPosition()).LengthSquared();
			if (squaredDistance < squaredRange)
			{
				T *pObject = dynamic_cast<T *>(pGameObject);
				if (pObject)
				{
					pClosest = pObject;
					squaredRange = squaredDistance;
				}
			}
		}

		return pClosest;
	}


protected:

	virtual CollisionManager *GetCollisionManager() { return m_pCollisionManager; }


private:
	int m_currentLevel;

	double m_counter = 0;
	double m_ScoreCount=0;

	CollisionManager *m_pCollisionManager = nullptr;

	std::vector<GameObject *> *m_pSectors;

	Vector2 m_sectorCount;
	Vector2 m_sectorSize;

	unsigned int m_totalSectorCount;

	std::vector<GameObject *> m_gameObjects;
	std::vector<GameObject *>::iterator m_gameObjectIt;

	PlayerShip *m_pPlayerShip;
	std::vector<Projectile *> m_projectiles;

	GameplayScreen* m_pGameplayScreen = nullptr;

	void CheckCollisions(std::vector<GameObject *> &sector);

	virtual Vector2 GetSectorCount() const { return m_sectorCount; }

	virtual Vector2 GetSectorSize() const { return m_sectorSize; }

	virtual unsigned int GetTotalSectorCount() const { return m_totalSectorCount; }

	virtual std::vector<GameObject *> *GetSectors() { return m_pSectors; }
};
