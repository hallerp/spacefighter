
#include "BioEnemyShip.h"
#include "Level.h"
#include "SpriteBatch.h"


class Ship;
class Level;
float Score;
BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150);
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	Score = GetCurrentLevel()->GetScore();
	if (IsActive())
	{
		// Adding in Random Movement
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition((Math::GetRandomFloat()+x) * 1.62, GetSpeed() * pGameTime->GetTimeElapsed());

		//Keeping Ships on Screen
		if (GetPosition().X < 100) 

			if (GetPosition().X > Game::GetScreenWidth() - 150) - x; 

		if (!IsOnScreen())
		{
			GetCurrentLevel()->IncrementCount(1);
			Deactivate();
			std::cout << GetCurrentLevel()->GetCount();
		}
	}


	EnemyShip::Update(pGameTime);
}

//Display Score tracker
void Game::DisplayScore()
{
	Color color(0, 1, 0);

	//float Score = 0;
	//Score=Level::GetCount();

	/*if (GameObject::GetCurrentLevel()->GetCount())
	{
		Score = GameObject::GetCurrentLevel()->GetCount();
	}*/

	char buffer[16];
	sprintf_s(buffer, "SCORE: %.0f", (float)Score);
	std::string text = buffer;

	m_pSpriteBatch->Begin();
	m_pSpriteBatch->DrawString(m_pScore, &text, Vector2(1300, 30), color);
	m_pSpriteBatch->End();

}



void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
