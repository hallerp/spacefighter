
#include "EnemyShip.h"
#include "Level.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}

void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
	
	if (GetCurrentLevel()->GetLevel() == 0)
	{
		GetCurrentLevel()->IncrementCount(1);
		GetCurrentLevel()->IncrementScore(100);
	}
	else if (GetCurrentLevel()->GetLevel() == 1)
	{
		GetCurrentLevel()->IncrementCount(.5);
		GetCurrentLevel()->IncrementScore(50);
	}

	
	std::cout << GetCurrentLevel()->GetCount();
	
}

