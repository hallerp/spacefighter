
#pragma once

#include "Weapon.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.35;

	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	// This fire the blaster
	// checks to see if blaster is active and if it can fire <- (based on cooldown)
	// checks the trigger type ie: primary, secondary, etc
	// creates the projectile
	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), true);
					if (GameObject::GetCurrentLevel()->GetLevel() == 0)
					{
						m_cooldown = m_cooldownSeconds;
					}
					else
					{
						m_cooldown = .15;
					}
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};